# import pickle
# import pandas as pd
# import numpy as np


# df_timing = pd.DataFrame(columns= ['total_time', 'monitoring_time', 'simulation_time', 'sample_generation_time', 'topk_time'])

# print(df_timing)


# for i in range(10):
#     with open(f"/home/local/ASUAD/tkhandai/RA_work/volvo_model/Volvo_lsemibo_test/Volvo_base/Volvo_base_falsification_elimination_point_history{i}.pkl", "rb") as  f:
#         data = pickle.load(f)
#     time_data = data["time_res"]    
#     # print(time_data.keys())
#     total_time = time_data["total_time"]
#     individual_monitoring_times = time_data["individual_monitoring_time"]
#     time_data.pop("individual_monitoring_time")
#     time_data.pop("total_time")

#     samp_gen_time = time_data["sample_generation_time"]

#     new_samp_gen_time = dict(zip(list(np.array(list(samp_gen_time.keys()))+101), samp_gen_time.values()))
#     time_data["sample_generation_time"] = new_samp_gen_time



#     df = pd.DataFrame(time_data)
#     # print(df_timing)
#     # print([total_time] + df.mean().to_list())
#     df_timing.loc[i] = [total_time] + df.sum().to_list()
    
import pickle
import pandas as pd
import numpy as np


df_timing = pd.DataFrame(columns= ['total_time', 'monitoring_time', 'simulation_time', 'ei_optimization_time', 'topk_time'])

print(df_timing)


for i in range(10):
    path = f"/home/local/ASUAD/tkhandai/RA_work/LS-emiBO/ArchBenchmarks_LSemiBO/LSemiBO_AT_timed_06012023/ATall/ATall_falsification_elimination_point_history{i}.pkl"
    # path = f"/home/local/ASUAD/tkhandai/RA_work/volvo_model/Volvo_lsemibo_test/Volvo_base/Volvo_base_falsification_elimination_point_history{i}.pkl"
    with open(path, "rb") as  f:
        data = pickle.load(f)
    print(data.keys())
    time_data = data["time_res"]    
    # print(time_data.keys())
    total_time = time_data["total_time"]
    individual_monitoring_times = time_data["individual_monitoring_time"]
    time_data.pop("individual_monitoring_time")
    time_data.pop("total_time")

    # samp_gen_time = time_data["sample_generation_time"]

    # new_samp_gen_time = dict(zip(list(np.array(list(samp_gen_time.keys()))+101), samp_gen_time.values()))
    # time_data["sample_generation_time"] = new_samp_gen_time



    df = pd.DataFrame(time_data)
    print(df)
    # print(df_timing)
    # print([total_time] + df.mean().to_list())
    df_timing.loc[i] = [total_time] + df.sum().to_list()
    
print(df_timing["total_time"].mean())
print(df_timing["total_time"].sem())

new_df = pd.DataFrame()
new_df["total_time(s)"] = df_timing["total_time"]

df_timing = df_timing.div(df_timing.iloc[:,0], axis = 0)
print(df_timing)
new_df["Simulation and Monitoring Time (%)"] = 100*(df_timing["monitoring_time"] + df_timing["simulation_time"])
new_df["Sample Generation Time(%)"] = 100*(df_timing.iloc[:,3] + df_timing.iloc[:,4])
new_df["Total Time (%)"] = 100*df_timing["total_time"]
new_df = new_df




new_df.loc["Mean"] = new_df.iloc[list(range(10)), :].mean().to_list()
new_df.loc["semDev"] = new_df.iloc[list(range(10)), :].sem().to_list()
print(new_df)  
new_df.to_csv("LSemiBO.csv", index = True)