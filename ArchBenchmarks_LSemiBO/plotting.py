import numpy as np
import pickle
from matplotlib import pyplot as plt


fals_data = {}
reps = 10
maxEval = 300
for seed in range(reps):
    # seed = 0
    path = f"/home/local/ASUAD/tkhandai/RA_work/LS-emiBO/ArchBenchmarks_LSemiBO/LSemiBO_AT_timed_06012023/ATall/ATall_falsification_elimination_point_history{seed}.pkl"
    with open(path, "rb") as f:
        data = pickle.load(f)

    # print(data)
    robvals = data["components"]

    fals_data[seed] = {}
    for i in range(5):
        fals_data[seed][i] = {}
        fals_data[seed][i]["num_eval"] = len(robvals[i])
        fals_data[seed][i]["falsified"] = 1 if len(robvals[i]) < maxEval else 0
    # plt.plot(list(range(len(robvals[0]))), robvals[0], label = "Sepc 0")
    # plt.plot(list(range(len(robvals[1]))), robvals[1], label = "Sepc 1")
    # plt.plot(list(range(len(robvals[2]))), robvals[2], label = "Sepc 2")
    # plt.plot(list(range(len(robvals[3]))), robvals[3], label = "Sepc 3")
    # plt.plot(list(range(len(robvals[4]))), robvals[4], label = "Sepc 4")
    # plt.legend()
    # plt.show()

fals_stat = {}

for spec in range(5):
    # fals_stat[spec]["fals_count"] = 0
    # fals_stat[spec]["fals_rate"] = 0
    mean_l = []
    fals_rate = 0
    for seed in range(reps):
        mean_l.append(fals_data[seed][spec]["num_eval"])
        fals_rate = (fals_rate + 1) if fals_data[seed][spec]["falsified"] == 1 else (fals_rate + 0)
    fals_stat[spec] = {}
    fals_stat[spec]["fals_count"] = mean_l
    fals_stat[spec]["mean_eval"] = np.mean(mean_l)
    fals_stat[spec]["median_eval"] = np.median(mean_l)
    # fals_stat[spec]["fals_rate"] = 0
    fals_stat[spec]["fals_rate"] = fals_rate

print(fals_stat)


for i in range(5):
    print("*********")
    print(fals_stat[i]["fals_rate"])   
    print(fals_stat[i]["mean_eval"])
    print(fals_stat[i]["median_eval"])   
     