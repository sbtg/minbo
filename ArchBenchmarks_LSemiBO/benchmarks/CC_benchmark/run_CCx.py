
import numpy as np

from models import AutotransModel
from Benchmark import Benchmark

from lsemibo.coreAlgorithm import LSemiBO
from lsemibo.gprInterface import InternalGPR
from lsemibo.classifierInterface import InternalClassifier

from staliro.options import Options, SignalOptions

# Define Signals and Specification
class Benchmark_CCx(Benchmark):
    def __init__(self, benchmark, results_folder) -> None:
        if benchmark != "CCx":
            raise ValueError("Inappropriate Benchmark name")

        self.benchmark = benchmark
        self.results_folder = results_folder
        
        
        phi_1 = "(G[0, 50] (y21 >= 7.5))"
        phi_2 = "(G[0, 50] (y32 >= 7.5))"
        phi_3 = "(G[0, 50] (y43 >= 7.5))"
        phi_4 = "(G[0, 50] (y54 >= 7.5))"

        self.is_budget = 50
        self.cs_budget = 1000
        self.MAX_BUDGET = 300
        self.NUMBER_OF_MACRO_REPLICATIONS = 10

        self.top_k = 3
        self.classified_sample_bias = 0.8
        self.tf_dim = 10

        self.phi_list = [phi_1, phi_2, phi_3, phi_4]
        self.pred_map = {"speed": ([0,1,2,3,4,5,6,7,8,9], 0), "rpm": ([0,1,2,3,4,5,6,7,8,9], 1)} #Change this

        self.R = 20
        self.M = 500
        
        
        self.model = AutotransModel()

        signals = [
            SignalOptions(control_points = [(0., 1.)] * 10, signal_times=np.linspace(0.0, 100.0, 10)),
            SignalOptions(control_points = [(0., 1.)] * 10, signal_times=np.linspace(0.0, 100.0, 10)),
        ]
        self.options = Options(runs=1, iterations=self.MAX_BUDGET, interval=(0, 100),  signals=signals)
        self.seed = 123456

    def run(self):
        
        for run in range(self.NUMBER_OF_MACRO_REPLICATIONS):
            seed = self.seed + run

            lsemibo = LSemiBO(
                benchmark_name=self.benchmark,
                folder_name = self.results_folder,
                run_number= run,
                is_budget = self.is_budget, 
                max_budget = self.MAX_BUDGET, 
                cs_budget = self.cs_budget, 
                top_k = self.top_k,
                classified_sample_bias=self.classified_sample_bias,
                model = self.model,
                component_list=self.phi_list,
                predicate_mapping=self.pred_map,
                tf_dim = self.tf_dim,
                options=self.options, 
                R = self.R, 
                M = self.M, 
                is_type = "lhs_sampling", 
                cs_type = "lhs_sampling", 
                seed = seed)

            x_train, y_train, time_taken = lsemibo.sample(InternalGPR(), InternalClassifier())

