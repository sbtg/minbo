import numpy as np

from models import AutotransModel
from Benchmark import Benchmark

from minbo.coreAlgorithm import MinBO
from minbo.gprInterface import InternalGPR
# from lsemibo.classifierInterface import InternalClassifier

from staliro.options import Options, SignalOptions

# Define Signals and Specification
class Benchmark_ATminBO(Benchmark):
    def __init__(self, benchmark, results_folder) -> None:
        if benchmark != "ATminBO":
            raise ValueError("Inappropriate Benchmark name")

        self.benchmark = benchmark
        self.results_folder = results_folder
        
        AT1_phi = "G[0, 20] (speed <= 120)"
        AT2_phi = "G[0, 10] (rpm <= 4750)"

        gear_1_phi = f"(gear <= 1.5 and gear >= 0.5)"
        AT51_phi = f"G[0, 30] (((not {gear_1_phi}) and (F[0.001,0.1] {gear_1_phi})) -> (F[0.001, 0.1] (G[0,2.5] {gear_1_phi})))"

        gear_2_phi = f"(gear <= 2.5 and gear >= 1.5)"
        AT52_phi = f"G[0, 30] (((not {gear_2_phi}) and (F[0.001,0.1] {gear_2_phi})) -> (F[0.001, 0.1] (G[0,2.5] {gear_2_phi})))"

        gear_3_phi = f"(gear <= 3.5 and gear >= 2.5)"
        AT53_phi = f"G[0, 30] (((not {gear_3_phi}) and (F[0.001,0.1] {gear_3_phi})) -> (F[0.001, 0.1] (G[0,2.5] {gear_3_phi})))"

        gear_4_phi = f"(gear <= 4.5 and gear >= 3.5)"
        AT54_phi = f"G[0, 30] (((not {gear_4_phi}) and (F[0.001,0.1] {gear_4_phi})) -> (F[0.001, 0.1] (G[0,2.5] {gear_4_phi})))"

        AT6a_phi = "((G[0, 30] (rpm <= 3000)) -> (G[0,4] (speed <= 35)))"
        AT6b_phi = "((G[0, 30] (rpm <= 3000)) -> (G[0,8] (speed <= 50)))"
        AT6c_phi = "((G[0, 30] (rpm <= 3000)) -> (G[0,20] (speed <= 65)))"

        self.is_budget = 100
        self.cs_budget = 1000
        self.MAX_BUDGET = 300
        self.NUMBER_OF_MACRO_REPLICATIONS = 4

        self.top_k = 2
        self.classified_sample_bias = 0.9
        self.tf_dim = 10

        self.phi_list = [AT1_phi, AT2_phi, AT6a_phi, AT6b_phi, AT6c_phi]
        # self.phi_list = [AT1_phi]
        self.pred_map = {"speed":([0,1,2,3,4,5,6,7,8,9], 0), 
                        "rpm":([0,1,2,3,4,5,6,7,8,9], 1),
                        "gear":([0,1,2,3,4,5,6,7,8,9], 2)
                    }

        self.R = 10
        self.M = 500
        
        
        self.model = AutotransModel()

        signals = [
            SignalOptions(control_points = [(0, 100)]*7, signal_times=np.linspace(0.,50.,7)),
            SignalOptions(control_points = [(0, 325)]*3, signal_times=np.linspace(0.,50.,3)),
            ]
        self.options = Options(runs=1, iterations=self.MAX_BUDGET, interval=(0, 50),  signals=signals)
        
        self.starting_seed = 123463

    def run(self):
        
        for run in range(self.NUMBER_OF_MACRO_REPLICATIONS):
            
            minbo_opt = MinBO(
                method = "falsification_elimination",
                benchmark_name=self.benchmark,
                folder_name = self.results_folder,
                run_number= run,
                is_budget = self.is_budget, 
                max_budget = self.MAX_BUDGET, 
                # cs_budget = self.cs_budget, 
                # top_k = self.top_k,
                # classified_sample_bias=self.classified_sample_bias,
                model = self.model,
                component_list=self.phi_list,
                predicate_mapping=self.pred_map,
                tf_dim = self.tf_dim,
                options=self.options, 
                # R = self.R, 
                # M = self.M, 
                is_type = "lhs_sampling", 
                # cs_type = "lhs_sampling",
                # pi_type = "lhs_sampling",
                starting_seed = self.starting_seed
            )

            out_data = minbo_opt.sample(InternalGPR())