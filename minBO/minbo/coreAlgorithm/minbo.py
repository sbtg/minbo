from tracemalloc import start
from .specification import Requirement
from ..sampling import uniform_sampling, lhs_sampling
from ..utils import compute_robustness, Evaluator, sample_spec
from ..gprInterface import GPR
from .specGPR import SpecEI


from pyswarms.single.global_best import GlobalBestPSO
from staliro.staliro import _signal_bounds
import numpy as np
from tqdm import tqdm
import pathlib
import pickle
import time
from scipy.stats import norm
from scipy import stats
from scipy.optimize import minimize

class MinBO:
    def __init__(self, method, benchmark_name, folder_name, run_number, is_budget, 
                max_budget, model, 
                component_list, predicate_mapping, tf_dim, options,  
                is_type = "lhs_sampling", starting_seed = 12345):

        self.run_number = run_number
        self.seed = starting_seed + run_number
        self.benchmark_name = benchmark_name
        self.method = method
        base_path = pathlib.Path()
        result_directory = base_path.joinpath(folder_name)
        result_directory.mkdir(exist_ok=True)
        self.benchmark_directory = result_directory.joinpath(benchmark_name)
        self.benchmark_directory.mkdir(exist_ok=True)


        self.bo_budget = max_budget - is_budget
        signal_bounds = _signal_bounds(options.signals)
        bounds = options.static_parameters + signal_bounds
        self.region_support = np.array((tuple(bound.astuple() for bound in bounds),))[0]
        self.rng = np.random.default_rng(self.seed)
        self.is_type = is_type
        self.is_budget = is_budget
       
        self.max_budget = max_budget
        self.rem_budget = self.max_budget - self.is_budget
        self.tf_dim = tf_dim
        
        self.num_requirements = len(component_list)

        self.requirement = Requirement(tf_dim, component_list, predicate_mapping)
        self.tf_wrapper = Evaluator(model, options)

        
    def sample(self, input_gpr_model):
        time_stats = {}
        start_time = time.perf_counter()
        print(f"Starting Replication for Run {self.run_number} with seed {self.seed}")

        if self.is_type == "lhs_sampling":
            x_train = lhs_sampling(self.is_budget, self.region_support, self.tf_dim, self.rng)
        elif self.is_type == "uniform_sampling":
            x_train = uniform_sampling(self.is_budget, self.region_support, self.tf_dim, self.rng)
        else:
            raise ValueError(f"{self.is_type} not defined. Currently only Latin Hypercube Sampling and Uniform Sampling is supported.")

        y_train = compute_robustness(x_train, self.requirement, self.tf_wrapper)
        ei_times = {}
        samp_generation_time = {}
        for budget in tqdm(range(self.max_budget - self.tf_wrapper.count)):
            
            print(f"Falsified Components: {self.requirement._get_num_falsified_comp()}\n Unfalsified components remaining: {self.requirement._get_num_unfalsified_comp()}")
            if self.requirement._get_num_unfalsified_comp() <= self.num_requirements:
                c1 = (self.requirement._get_num_unfalsified_comp() == 0 and self.method == "falsification_elimination")
                c2 = (self.requirement._get_num_unfalsified_comp() < self.num_requirements and self.method == "falsification")
                if c1 or c2:
                    # elapsed_time = time.perf_counter() - start_time
                    time_stats["total_time"] = time.perf_counter() - start_time
                    time_stats["monitoring_time"] = self.tf_wrapper.monitoring_time
                    time_stats["simulation_time"] = self.tf_wrapper.sim_time
                    time_stats["individual_monitoring_time"] = self.requirement._get_individual_monitoring_times()
                    time_stats["sample_generation_time"] = samp_generation_time
                    time_stats["ei_times"] = ei_times

                    print("All reqs falsified")
                    print(f"Ending Replication Early for Run {self.run_number} with seed {self.seed}")
                    if c1:
                        print("Ending early due to elimination of all reqs")
                    elif c2:
                        print("Ending early due to falsification")
                    output_data = {}
                    output_data["samples"] = x_train
                    output_data["components"] = self.requirement._get_complete_data()
                    output_data["time_stats"] = time_stats
                    print(f"Ending Replication After Exhuasting budget for Run {self.run_number} with seed {self.seed}")
                    with open(self.benchmark_directory.joinpath(self.benchmark_name + f"_{self.method}_point_history{self.run_number}.pkl"), "wb") as f:
                        pickle.dump(output_data, f)
                    
                    return output_data
                else:
                    # curr_req = self.requirements[self._select_requirement(curr_active_specs)]
                    
                    
                    pred_sample_x, _indi_ei_time, _samp_generation_time = self.minbo_req(x_train, input_gpr_model)
                    ei_times[self.is_budget+budget+1] = _indi_ei_time
                    samp_generation_time[self.is_budget+budget+1] = _samp_generation_time
                    x_train = np.vstack((x_train, np.array([pred_sample_x])))
                    pred_sample_y = compute_robustness(np.array([pred_sample_x]), self.requirement, self.tf_wrapper)
                    print(pred_sample_y)
                    
        
        time_stats["total_time"] = time.perf_counter() - start_time
        time_stats["monitoring_time"] = self.tf_wrapper.monitoring_time
        time_stats["simulation_time"] = self.tf_wrapper.sim_time
        time_stats["individual_monitoring_time"] = self.requirement._get_individual_monitoring_times()
        time_stats["sample_generation_time"] = samp_generation_time
        time_stats["ei_times"] = ei_times
        output_data = {}
        output_data["samples"] = x_train
        output_data["components"] = self.requirement._get_complete_data()
        output_data["time_stats"] = time_stats
        print(f"Ending Replication After Exhuasting budget for Run {self.run_number} with seed {self.seed}")
        with open(self.benchmark_directory.joinpath(self.benchmark_name + f"_{self.method}_point_history{self.run_number}.pkl"), "wb") as f:
            pickle.dump(output_data, f)

        return output_data

    def minbo_req(self, x_train, gpr_model):
        idxs, y_train = self.requirement._generate_dataset()
        
        idxs_dict = dict(zip(idxs, list(range(len(idxs)))))

        component_ei = []
        potential_pred_x = []
        best_point = np.min(y_train)
        pi = []
        indi_ei_time = {}
        start_samp_generation_point = time.perf_counter()
        for iterate in idxs:
            mapping_indices = np.where(self.requirement.requirements[iterate].io_mapping == 1)[0]
            start_indi_ei_time = time.perf_counter()
            x_train_subset = x_train[:, mapping_indices]
            y_train_subset = y_train[:, idxs_dict[iterate]]
            
            req_comp = SpecEI(iterate, x_train_subset, y_train_subset, best_point, mapping_indices, gpr_model, self.rng)
            

            params = self._opt_acquisition(req_comp, self.region_support, self.tf_dim)
            component_ei.append(params.fun)
            potential_pred_x.append(params.x)
            indi_ei_time[self.requirement.requirements[iterate].id] = time.perf_counter() - start_indi_ei_time

        print(component_ei)
        
        
        return np.array(potential_pred_x[np.argmin(component_ei)]), indi_ei_time, time.perf_counter() - start_samp_generation_point


    def _opt_acquisition(self, spec_ei, region_support, tf_dim):
        """Get the sample points

        Args:
            X (np.array): sample points
            y (np.array): corresponding robustness values
            model ([type]): the GP models
            sbo (list): sample points to construct the robustness values
            test_function_dimension (int): The dimensionality of the region. (Dimensionality of the test function)
            region_support (np.array): The bounds of the region within which the sampling is to be done.
                                        Region Bounds is M x N x O where;
                                            M = number of regions;
                                            N = test_function_dimension (Dimensionality of the test function);
                                            O = Lower and Upper bound. Should be of length 2;

        Returns:
            [np.array]: the new sample points by BO
            [np.array]: sbo - new samples for resuse
        """

        lower_bound_theta = np.ndarray.flatten(region_support[:, 0])
        upper_bound_theta = np.ndarray.flatten(region_support[:, 1])
        bounds = (lower_bound_theta, upper_bound_theta)
        
        # bnds = Bounds(lower_bound_theta, upper_bound_theta)
        fun = lambda _x: -1 * spec_ei._acquisition(_x, "single")
        # t = time.time()
        random_samples = uniform_sampling(2000, region_support, tf_dim, self.rng)
        min_bo_val = -1 * spec_ei._acquisition(
            random_samples, sample_type="multiple"
        )

        min_bo = np.array([random_samples[np.argmin(min_bo_val), :]])
        min_bo_val = np.min(min_bo_val)

        
        for _ in range(9):
            new_params = minimize(
                fun,
                bounds=list(zip(lower_bound_theta, upper_bound_theta)),
                method = "L-BFGS-B",
                x0=min_bo,
            )
            # print(new_params)

            if not new_params.success:
                continue

            if min_bo is None or fun(new_params.x) < min_bo_val:
                min_bo = new_params.x
                min_bo_val = fun(min_bo)

        new_params = minimize(
            fun, bounds=list(zip(lower_bound_theta, upper_bound_theta)), x0=min_bo
        )
        
        # min_bo = new_params.x
        
        return new_params

    
    