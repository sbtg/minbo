from ..gprInterface import GPR
from scipy.stats import norm
from copy import deepcopy
import numpy as np

class SpecEI:
    def __init__(self, identifier, x_train, y_train, best_point, mapping_indices, gpr_model, rng):
        self.id = identifier
        self.x_train = x_train
        self.y_train = y_train
        self.mapping_indices = mapping_indices
        self.rng = rng
        self.model = deepcopy(GPR(gpr_model))
        self.model.fit(self.x_train, self.y_train)
        self.best_point = best_point
        

    def _surrogate(self, x_train):
        """_surrogate Model function

        Args:
            model: Gaussian process model
            X: Input points

        Returns:
            [type]: predicted values of points using gaussian process model
        """

        return self.model.predict(x_train)

    def _acquisition(self, sample, sample_type="single"):
        
        if len(sample.shape) == 1:
            sample = sample.reshape((-1,1)).T


        sample_subset = sample[:, self.mapping_indices]
        curr_best = self.best_point
        
        
        if sample_type == "multiple":
            
            mu, std = self._surrogate(sample_subset)
            ei_list = []
            for mu_iter, std_iter in zip(mu, std):
                pred_var = std_iter
                
                if pred_var > 0:
                    var_1 = curr_best - mu_iter
                    var_2 = var_1 / pred_var

                    ei = (var_1 * norm.cdf(var_2)) + (
                        pred_var * norm.pdf(var_2)
                    )
                else:
                    ei = 0.0

                ei_list.append(ei)

            return np.array(ei_list)

        elif sample_type == "single":
            mu, std = self._surrogate(sample_subset.reshape(1, -1))
            
            pred_var = std[0]
            if pred_var > 0:
                var_1 = curr_best - mu[0]
                var_2 = var_1 / pred_var

                ei = (var_1 * norm.cdf(var_2)) + (
                    pred_var * norm.pdf(var_2)
                )
            else:
                ei = 0.0
            return ei